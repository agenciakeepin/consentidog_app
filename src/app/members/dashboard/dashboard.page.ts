import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { PetService } from '../../services/petservice.service';
import { Observable } from 'rxjs';
 
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss']
})
export class DashboardPage implements OnInit {

  public currentServices: Observable<any>;

  constructor(
    private authService: AuthenticationService,
    private PetService: PetService
  ) { }

  /*toArray(breeds: object) {
    return Object.keys(breeds).map(key => breeds[key])
  }
  */  
 
  ngOnInit() {
    //console.log('dashboard.page.ts');
    //this.currentServices =  this.PetService.getCurServices();
  }

  

  ionViewDidEnter(){ 
    document.addEventListener("backbutton",function(e) {
      console.log("Disable back button")
    }, false);
    //this.PetService.datoslog().finally();
    this.currentServices =  this.PetService.getCurServices();

    
  }

  consulta(){
    
  }

  logout() {
    this.authService.logout();
  }
}