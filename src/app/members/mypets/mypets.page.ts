import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Pet } from '../../services/pet.service';
import { Observable } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-mypets',
  templateUrl: './mypets.page.html',
  styleUrls: ['./mypets.page.scss'],
})
export class MypetsPage implements OnInit {

  public myPets: Observable<any>;
  urlroot = '';

  constructor(
    private authService: AuthenticationService,
    private Pets: Pet,
    private router: Router
  ) { }

  ngOnInit() {
    let posapi = this.authService.baseurl.indexOf("/api");
    this.urlroot = this.authService.baseurl.substr(0,posapi);
    //this.urlroot = this.authService.baseurl;
    this.myPets =  this.Pets.getMyPets();
  }

  ionViewDidEnter(){ 
    
  }

  editpet(pet:any){
    let navigationExtras: NavigationExtras = {
      state: pet
    };
    this.router.navigate(['/members/editpet'], navigationExtras)
  }

  logout() {
    this.authService.logout();
  }

}
