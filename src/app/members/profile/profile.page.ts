import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
//import { PetService } from '../../services/petservice.service';
import { User } from '../../services/user.service';
import { Observable } from 'rxjs';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  infousu: Observable<any>;

  datosusu = {
    id: '',
    email: '',
    username: '',
    name: '',
    address_line_1: '',
    address_line_2: '',
    city_id: '',
    postal_code: '',
    phone: '',
    mobile: ''
  };

  constructor(
    private authService: AuthenticationService,
    //private PetService: PetService,
    private User: User,
    public translate: TranslateService
    ) { }

  ngOnInit() {
    //this.infousu = this.PetService.getusuinfo();
    this.infousu = this.User.getusuinfo();
    this.infousu.subscribe( 
      res => {
        this.datosusu.id = res.id,
        this.datosusu.email = res.email,
        this.datosusu.username = res.username,
        this.datosusu.name = res.name,
        this.datosusu.address_line_1 = res.address_line_1,
        this.datosusu.address_line_2 = res.address_line_2,
        this.datosusu.city_id = res.city_id,
        this.datosusu.postal_code = res.postal_code,
        this.datosusu.phone = res.phone,
        this.datosusu.mobile = res.mobile
      }
    );
    //console.log(this.infousu);
  }

  logout() {
    this.authService.logout();
  }

}
