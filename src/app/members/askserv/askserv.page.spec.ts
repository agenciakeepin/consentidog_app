import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AskservPage } from './askserv.page';

describe('AskservPage', () => {
  let component: AskservPage;
  let fixture: ComponentFixture<AskservPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AskservPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AskservPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
