import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';

import { Pet } from '../../services/pet.service';
import { Breeds } from '../../services/breeds.service';
import { Services } from '../../services/services.service';
//import { Genders } from '../../services/genders.service';
//import { Colours } from '../../services/colours.service';
import { User } from '../../services/user.service';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ToastController } from '@ionic/angular';
import { LoaderService } from '../../services/loader.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-askserv',
  templateUrl: './askserv.page.html',
  styleUrls: ['./askserv.page.scss'],
})
export class AskservPage implements OnInit {

  public breedslist: Observable<any>;
  public serviceslist: Observable<any>;
  //public genderslist: Observable<any>;
  //public colourslist: Observable<any>;
  public myPets: Observable<any>;

  private fechahoy: string = Date.now().toString();

  infousu: Observable<any>;

  datosserv = {
    service: '',
    llegada: '',
    retiro: '',
    username: this.user.name.toString(),
    userphone: '',
    usermail: '',
    //user_id: this.user.idusuario.toString(),
    petname: '',
    petbreed: ''
  };

  constructor(
    public translate: TranslateService,
    private authService: AuthenticationService,
    private http: HttpClient,
    //private sanitizer: DomSanitizer,
    public toastController: ToastController,
    //private loadServ: LoaderService,
    private router: Router,
    private pets: Pet,
    private breeds: Breeds,
    private services: Services,
    //private genders: Genders,
    //private colours: Colours,
    private user: User
  ) { 
    
  }

  ngOnInit() {

  }

  ionViewDidEnter(){ 
    this.datosserv.llegada = this.fechahoy;
    this.datosserv.retiro = this.fechahoy;

    this.infousu = this.user.getusuinfo();
    this.infousu.subscribe( 
      res => {
        this.datosserv.username = res.name,
        this.datosserv.usermail = res.email,
        this.datosserv.userphone = res.mobile
      }
    );
    this.serviceslist = this.services.getServices();
    this.myPets =  this.pets.getMyPets(); 
    this.breedslist =  this.breeds.getBreeds();
    //this.genderslist =  this.genders.getGenders();
    //this.colourslist = this.colours.getColours();
  }

  enviareserva(){
    

    let postData = new FormData();
    postData.set("service",this.datosserv.service);
    postData.set("llegada",this.datosserv.llegada);
    postData.set('retiro', this.datosserv.retiro);
    postData.set("username", this.datosserv.username);
    postData.set("userphone", this.datosserv.userphone);
    postData.set("usermail", this.datosserv.usermail);
    postData.set("petname", this.datosserv.petname);
    postData.set("petbreed", this.datosserv.petbreed);

    /*let postData:  {
      username: '',
      password: ''   ,
          'Content-Type': 'application/json'
    };*/

    

    //console.log(this.datosserv);
    console.log(postData);

    return this.http
      .post<any>("https://qa.keepinagency.com/consentidog_app/enviareserva_app.php", postData, {
      //.post("http://192.168.2.105:8888/consentidog/consentidog_app/enviareserva_app.php", postData, {
        headers: new HttpHeaders({
          'Accept': 'application/json'})
        })
      .subscribe(data => {
        console.log(data['success']);
        console.log(data['message']);
        console.log(data['post']);
        this.presentToast(data['message']);
        if (data['success']){
          this.router.navigate(['/members/dashboard'])
        }
       }, error => {
        console.log(error);
      });
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      header: '',
      message: msg, //'Verifique los datos e intente nuevamente',
      duration: 5000, 
      position: 'top',
      color: 'secondary',
    });
    toast.present();
  }

  logout() {
    this.authService.logout();
  }

}
