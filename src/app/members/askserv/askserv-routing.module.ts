import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AskservPage } from './askserv.page';

const routes: Routes = [
  {
    path: '',
    component: AskservPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AskservPageRoutingModule {}
