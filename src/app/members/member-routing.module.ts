import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
 
//console.log('member-routing.module');

const routes: Routes = [
  { 
    path: 'dashboard', 
    //loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
    loadChildren: () => import('./dashboard/dashboard.module').then( m => m.DashboardPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'mypets',
    loadChildren: () => import('./mypets/mypets.module').then( m => m.MypetsPageModule)
  },
  {
    path: 'addpet',
    loadChildren: () => import('./addpet/addpet.module').then( m => m.AddpetPageModule)
  },
  {
    path: 'askserv',
    loadChildren: () => import('./askserv/askserv.module').then( m => m.AskservPageModule)
  },
  {
    path: 'editpet',
    loadChildren: () => import('./editpet/editpet.module').then( m => m.EditpetPageModule)
  }
];
 

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})

export class MemberRoutingModule { }