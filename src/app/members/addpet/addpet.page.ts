import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { Pet } from '../../services/pet.service';
import { Breeds } from '../../services/breeds.service';
import { Genders } from '../../services/genders.service';
import { Colours } from '../../services/colours.service';
import { User } from '../../services/user.service';

import { ToastController } from '@ionic/angular';
import { LoaderService } from '../../services/loader.service';
import { TranslateService } from '@ngx-translate/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

//import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';

@Component({
  selector: 'app-addpet',
  templateUrl: './addpet.page.html',
  styleUrls: ['./addpet.page.scss'],
})
export class AddpetPage implements OnInit {

  public breedslist: Observable<any>;
  public genderslist: Observable<any>;
  public colourslist: Observable<any>;

  private fechahoy: string = Date.now().toString();


  datospet = {
    name: '',
    gender_id: '0',
    sterile: '',
    birth_date: '',
    weight: '',
    user_id: this.user.idusuario.toString(),
    colour_id: '0',
    breed_id: '0',
    photo64:'',
    additional_information:''
  };

  photo: SafeResourceUrl;

  photo64: string = '';

  image: any = '';

  constructor(
    public translate: TranslateService,
    private authService: AuthenticationService,
    //private sanitizer: DomSanitizer,
    public toastController: ToastController,
    private loadServ: LoaderService,
    private router: Router,
    private pets: Pet,
    private breeds: Breeds,
    private genders: Genders,
    private colours: Colours,
    private user: User
    //private androidPermissions: AndroidPermissions
  ) { }

  ngOnInit() {
    
  }

  ionViewDidEnter(){ 
    this.breedslist =  this.breeds.getBreeds();
    this.genderslist =  this.genders.getGenders();
    this.colourslist = this.colours.getColours();
  }

  async takePicture() {
    /*this.androidPermissions.checkPermission(this.androidPermissions.PERMISSION.CAMERA).then(
      result => console.log('Has permission?',result.hasPermission),
      err => this.androidPermissions.requestPermission(this.androidPermissions.PERMISSION.CAMERA)
    );
    
    this.androidPermissions.requestPermissions([this.androidPermissions.PERMISSION.CAMERA, this.androidPermissions.PERMISSION.GET_ACCOUNTS]);
    */ 
    this.image = await Plugins.Camera.getPhoto({
      quality: 100,
      allowEditing: false,
      resultType: CameraResultType.Base64, //DataUrl, Uri, //
      source: CameraSource.Camera
    });
    //console.log(this.image);
    this.datospet.photo64 = this.image.base64String;
    
    //this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(this.image && (this.image.dataUrl));
    this.photo = 'data:image/jpeg;base64,' + this.image.base64String;
  }

  async doAddPet(){
    this.loadServ.show('Verificando información...');
    console.log(this.datospet);
    //this.datospet.birth_date = this.datospet.birth_date + " 00:00:00";
    this.pets.addpet(this.datospet).subscribe(
      res => {
        this.loadServ.hide();
        if (res.success) {
          return true;
        }else{
          return false;
        };
         
      },
      err => {
        //console.log('222222');
        this.loadServ.hide(); 

        let errores     = Object.entries(err.error.data.errors);
        let cmps_vacios = "";
        let cmps_unique = "";
        let cmps_invalid = "";
        let mensaje     = "";

        console.log(err);

        errores.forEach( ([campo, arrerror]) => {

          let msgerror = Object.entries(arrerror);

          msgerror.forEach( ([iderror, mensaje]) => {

            if (iderror=="_empty"){
              cmps_vacios = cmps_vacios + this.translate.instant(campo.toUpperCase()) + ", ";
            }else{
              if (iderror=="unique"){
                cmps_unique = cmps_unique + this.translate.instant(campo.toUpperCase()) + " ";
              }else{
                if (iderror=="dateTime" || iderror=="_existsIn" || iderror=="custom"){
                  cmps_invalid = cmps_invalid + this.translate.instant(campo.toUpperCase()) + " ";
                }
                // Otros mensaje... considerar!
              }
            }
          });

        });
        if ( cmps_vacios.length>0 && cmps_unique.length>0){
          mensaje = this.translate.instant("unique_error") + "<br><br> <center><b>" + cmps_unique + "</b></center>";
          //mensaje = mensaje + "Debe completar los campos " + cmps_vacios + ", adicionalmente";
        }else{
          if (cmps_vacios.length>0){
            mensaje = this.translate.instant("_empty_error") + "<br><br> <center><b>" + cmps_vacios + "</b></center>";
          }else{
            if (cmps_invalid.length>0){
              mensaje = this.translate.instant("type_error") + "<br><br> <center><b>" + cmps_invalid + "</b></center>";
            }else{
              mensaje = this.translate.instant("unknow_error");
            }
            
          }
        }
        this.presentToast(mensaje);

      },
      () => {
        this.presentToast(this.translate.instant("pet_added"));
        console.log('AddPet completed.')
        this.router.navigate(['/members/mypets'])
      }
    );
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      header: '',
      message: msg, //'Verifique los datos e intente nuevamente',
      duration: 5000, 
      position: 'top',
      color: 'secondary',
    });
    toast.present();
  }

  logout() {
    this.authService.logout();
  }
}
