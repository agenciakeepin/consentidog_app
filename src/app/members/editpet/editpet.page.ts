import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { Plugins, CameraResultType, CameraSource, CameraDirection } from '@capacitor/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { LoaderService } from '../../services/loader.service';
import { ToastController } from '@ionic/angular';

import { Breeds } from '../../services/breeds.service';
import { Genders } from '../../services/genders.service';
import { Colours } from '../../services/colours.service';
import { Pet } from '../../services/pet.service';

import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';



@Component({
  selector: 'app-editpet',
  templateUrl: './editpet.page.html',
  styleUrls: ['./editpet.page.scss'],
})
export class EditpetPage implements OnInit {

  public breedslist: Observable<any>;
  public genderslist: Observable<any>;
  public colourslist: Observable<any>;
  urlroot = '';

  data: any;
  
  photo: SafeResourceUrl;

  photo64: string = '';

  image: any = '';

  /*
  data:any = {
    id: '',
    name: '',
    gender_id: '0',
    sterile: '',
    birth_date: '',
    weight: '',
    user_id: '0',
    colour_id: '0',
    breed_id: '0',
    photo64:'',
    additional_information:''
  };
  */

  constructor(
    public translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    private authService: AuthenticationService,
    private breeds: Breeds,
    private genders: Genders,
    private colours: Colours,
    private loadServ: LoaderService,
    public toastController: ToastController,
    private pets: Pet,
    private location: Location
  ) {
    

    this.breeds.getBreeds()
      .subscribe((data: any) => {
        //console.log(data);
        this.breedslist = data;
      },
      (error: any) => {
          console.log(error);
      });
    
    // 
    //this.breedslist =  this.breeds.getBreeds();
    this.colourslist = this.colours.getColours();
    
   }

  ngOnInit() {
    /*this.genders.getGenders()
      .subscribe((data: any) => {
        console.log('Genders en editpet.page.ts');
        console.log(data);
        this.genderslist = data;
      },
      (error: any) => {
          console.log(error);
      });*/
    //this.genderslist = this.genders.getGenders();


    let posapi = this.authService.baseurl.indexOf("/api");
    this.urlroot = this.authService.baseurl.substr(0,posapi); 

    this.route.queryParams.subscribe(params => {
      //console.log(this.router.getCurrentNavigation());
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state;
        /*let petdata = this.router.getCurrentNavigation().extras.state;
        console.log('petdata ngOnInit edit.page.ts:');
        console.log(petdata);

        this.data.name = petdata.name;
        this.data.sterile = petdata.sterile;
        this.data.photo = petdata.photo;
        this.data.photo_dir = petdata.photo_dir;
        this.data.additional_information = petdata.additional_information;
        this.data.weight = petdata.weight;
        this.data.gender_id = petdata.gender_id;*/

        delete this.data.birth_date;
        //delete this.data.breeds;
        //delete this.data.gender;

        /*console.log('this.data ngOnInit edit.page.ts:');
        console.log(this.data);*/



        if (this.data.sterile == false){
          this.data.sterile = 0;
        }

        // {{urlroot}}/files/pets/photo/{{Pet.photo_dir}}/{{Pet.photo}}
        let urlfile = this.urlroot + '/files/pets/photo/'+ this.data.photo_dir + '/' + this.data.photo;
        this.getBase64ImageFromUrl(urlfile)
          .then(result => {
              //this.photo = result;
              this.photo = 'data:image/jpeg;base64,' + result;
              this.data.photo64 = result;
              })
          .catch(err => console.error(err));
      }
    });

  }

  ionViewDidEnter(){ 
    /*setTimeout(() => {
      //this.data.gender_id=0;
      console.log(this.data);
      this.photo = 'data:image/jpeg;base64,' + this.data.photo64;
    }, 1500);*/
    
  }

  async takePicture() {
    this.image = await Plugins.Camera.getPhoto({
      quality: 90,
      correctOrientation: false,
      direction: CameraDirection.Rear,
      allowEditing: true,
      resultType: CameraResultType.Base64, //DataUrl, Uri, //
      source: CameraSource.Prompt //Camera
    });
    //console.log(this.image);
    this.data.photo64 = this.image.base64String;
    
    //this.photo = this.sanitizer.bypassSecurityTrustResourceUrl(this.image && (this.image.dataUrl));
    this.photo = 'data:image/jpeg;base64,' + this.image.base64String;
  }

  async doEditPet(){
    this.loadServ.show('Verificando información...');
    console.log(this.data);
    this.data.photo64 = this.photo;
    //this.datospet.birth_date = this.datospet.birth_date + " 00:00:00";
    this.pets.editpet(this.data.id,this.data).subscribe(
      res => {
        this.loadServ.hide();
        if (res.success) {
          return true;
        }else{
          return false;
        };
         
      },
      err => {
        //console.log('222222');
        this.loadServ.hide(); 

        let errores     = Object.entries(err.error.data.errors);
        let cmps_vacios = "";
        let cmps_unique = "";
        let cmps_invalid = "";
        let mensaje     = "";

        console.log(err);

        errores.forEach( ([campo, arrerror]) => {

          let msgerror = Object.entries(arrerror);

          msgerror.forEach( ([iderror, mensaje]) => {

            if (iderror=="_empty"){
              cmps_vacios = cmps_vacios + this.translate.instant(campo.toUpperCase()) + ", ";
            }else{
              if (iderror=="unique"){
                cmps_unique = cmps_unique + this.translate.instant(campo.toUpperCase()) + " ";
              }else{
                if (iderror=="dateTime" || iderror=="_existsIn" || iderror=="custom"){
                  cmps_invalid = cmps_invalid + this.translate.instant(campo.toUpperCase()) + " ";
                }
                // Otros mensaje... considerar!
              }
            }
          });

        });
        if ( cmps_vacios.length>0 && cmps_unique.length>0){
          mensaje = this.translate.instant("unique_error") + "<br><br> <center><b>" + cmps_unique + "</b></center>";
          //mensaje = mensaje + "Debe completar los campos " + cmps_vacios + ", adicionalmente";
        }else{
          if (cmps_vacios.length>0){
            mensaje = this.translate.instant("_empty_error") + "<br><br> <center><b>" + cmps_vacios + "</b></center>";
          }else{
            if (cmps_invalid.length>0){
              mensaje = this.translate.instant("type_error") + "<br><br> <center><b>" + cmps_invalid + "</b></center>";
            }else{
              mensaje = this.translate.instant("unknow_error");
            }
            
          }
        }
        this.presentToast(mensaje);

      },
      () => {
        this.presentToast(this.translate.instant("pet_edited"));
        console.log('EditPet completed.')
        this.router.navigate(['/members/mypets'])
      }
    );
  }

  async getBase64ImageFromUrl(imageUrl) {
    var res = await fetch(imageUrl);
    var blob = await res.blob();
  
    return new Promise((resolve, reject) => {
      var reader  = new FileReader();
      reader.addEventListener("load", function () {
          resolve(reader.result);
      }, false);
  
      reader.onerror = () => {
        return reject(this);
      };
      reader.readAsDataURL(blob);
    })
  }

  goBack() {
    this.location.back();
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      header: '',
      message: msg, //'Verifique los datos e intente nuevamente',
      duration: 5000, 
      position: 'top',
      color: 'secondary',
    });
    toast.present();
  }

  logout() {
    this.authService.logout();
  }

}
