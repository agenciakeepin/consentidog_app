import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditpetPage } from './editpet.page';

describe('EditpetPage', () => {
  let component: EditpetPage;
  let fixture: ComponentFixture<EditpetPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditpetPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditpetPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
