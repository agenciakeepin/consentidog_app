import { Router } from '@angular/router';
import { AuthenticationService } from './services/authentication.service';
import { Component } from '@angular/core';
 
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { TranslateService } from '@ngx-translate/core';
 
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  navigate: any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private router: Router,
    private translate: TranslateService
  ) {
    this.sideMenu();
    this.initTranslate();
    this.initializeApp();
    
  }

  initializeApp() {
    this.platform.ready().then(() => {
    this.statusBar.styleDefault();
    this.splashScreen.hide();

    //console.log(this.authenticationService.isAuthenticated(),'isAuthenticated en app.component');

    /* 
    if (this.authenticationService.isAuthenticated()){
      let a = this.router.navigate(['members/dashboard'])
      console.log(a,'return de navigate en app.component');
    }else{
      this.router.navigate(['login']);
    }
    */

    this.authenticationService.authenticationState.subscribe(state => {
        //console.log(state,"state en app.component.ts");
        if (state) {
          //this.router.navigate(['members', 'dashboard']);
          //console.log(state);
          this.router.navigate(['members/dashboard'])
          //console.log(a,'return de navigate en app.component');
        } else {
          this.router.navigate(['login']);
        }
      });
    });
  }

  sideMenu()
  {
    this.navigate =
    [
      {
        title : "MNU_SERV",
        url   : "/members/dashboard",
        icon  : "receipt-outline"
      },
      {
        title : "MNU_MYPROFILE",
        url   : "/members/profile",
        icon  : "person-outline"
      },
      {
        title : "MNU_PETS",
        url   : "/members/mypets",
        icon  : "paw-outline"
      },
      /*{
        title : "MNU_SERV",
        url   : "/members/pets-services",
        icon  : "newspaper-outline"
      },*/
    ]
  }

  initTranslate() {
    // Set the default language for translation strings, and the current language.
    this.translate.setDefaultLang('en');
    const browserLang = this.translate.getBrowserLang();

    if (browserLang) {
      if (browserLang === 'zh') {
        const browserCultureLang = this.translate.getBrowserCultureLang();

        if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
          this.translate.use('zh-cmn-Hans');
        } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
          this.translate.use('zh-cmn-Hant');
        }
      } else {
        this.translate.use(this.translate.getBrowserLang());
      }
    } else {
      this.translate.use('es'); // Set your language here
    }

  }
}
