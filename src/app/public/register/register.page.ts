import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

import { User } from '../../services/user.service';
import { ToastController } from '@ionic/angular';
//import { TransferState } from '@angular/platform-browser';

import { LoaderService } from '../../services/loader.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {

  datosusu = {
    id: '',
    email: '',
    username: '',
    password: '',
    name: '',
    address_line_1: '',
    address_line_2: '',
    city_id: '1',
    postal_code: '',
    phone: '',
    mobile: '',
    role_id: '2',
    active: '1'
  };

  constructor(
    public translate: TranslateService,
    public toastController: ToastController,
    private loadServ: LoaderService,
    private router: Router,
    private user: User
    ){ }

  ngOnInit() {
  }

  doRegister(){
    this.loadServ.show('Verificando información...');
    this.user.adduser(this.datosusu).subscribe(
      res => {
        this.loadServ.hide();
        if (res.success) {
          return true;
        }else{
          return false;
        };
         
      },
      err => {
        this.loadServ.hide(); 

        let errores     = Object.entries(err.error.data.errors);
        let cmps_vacios = "";
        let cmps_unique = "";
        let mensaje     = "";

        errores.forEach( ([campo, arrerror]) => {

          let msgerror = Object.entries(arrerror);

          msgerror.forEach( ([iderror, mensaje]) => {

            if (iderror=="_empty"){
              cmps_vacios = cmps_vacios + this.translate.instant(campo.toUpperCase()) + ", ";
            }else{
              if (iderror=="unique"){
                cmps_unique = cmps_unique + this.translate.instant(campo.toUpperCase()) + " ";
              }else{
                // Otros mensaje... considerar!
              }
            }

          });

        });
        if ( cmps_vacios.length>0 && cmps_unique.length>0){
          mensaje = this.translate.instant("unique_error") + "<br><br> <center><b>" + cmps_unique + "</b></center>";
          //mensaje = mensaje + "Debe completar los campos " + cmps_vacios + ", adicionalmente";
        }else{
          if (cmps_vacios.length>0){
            mensaje = this.translate.instant("_empty_error") + "<br><br> <center><b>" + cmps_vacios + "</b></center>";
          }else{
            mensaje = this.translate.instant("unknow_error");
          }
        }
        this.presentToast(mensaje);

      },
      () => {
        this.presentToast(this.translate.instant("user_added"));
        console.log('AddUser completed.')
        this.router.navigate(['./login'])
      }
    );
  }

  async presentToast(msg: string) {
    const toast = await this.toastController.create({
      header: '',
      message: msg, //'Verifique los datos e intente nuevamente',
      duration: 5000, 
      position: 'top',
      color: 'secondary',
    });
    toast.present();
  }
}
