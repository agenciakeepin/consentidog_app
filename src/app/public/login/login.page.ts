import { AuthenticationService } from './../../services/authentication.service';
import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
 
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loading = false;

  account: { username: string, password: string } = {
    username: '',
    password: ''
  };

  constructor(
    private authService: AuthenticationService,
    public translate: TranslateService
  ) {}

  ngOnInit() {
  }
 
  login() {
    this.loading = true;
    let a = this.authService.login(this.account);
    //console.log(a,'login en login.page.ts');
  }

}