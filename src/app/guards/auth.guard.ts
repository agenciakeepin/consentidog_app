import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public auth: AuthenticationService) { 
    //console.log('entró en guard');
  }
  canActivate(): boolean {
    //console.log(this.auth.isAuthenticated(),'isAuthenticated en guard')
    return this.auth.isAuthenticated();
  }
}