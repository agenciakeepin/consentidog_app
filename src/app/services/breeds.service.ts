import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user.service';

import { AuthenticationService } from './authentication.service';
import { Storage } from '@ionic/storage';

import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class Breeds {

  private url: string = this.authService.baseurl + 'breeds';
  //private url: string = 'http://192.168.2.105:8888/consentidog/consentidog_backend/api/breeds';
  //url: string = 'https://qa.keepinagency.com/consentidog_backend/v0.1.1/api/users';

  constructor(
    private loadServ: LoaderService,
    private http: HttpClient,
    private authService: AuthenticationService,
    private storage: Storage, 
    private user: User
  ) { }

  getBreeds(): Observable<object> {
    //
    
    this.storage.get('auth-token').then(
      res => {
        this.user.token = res;
      }
    );

    if (this.user.token.length>1){
      return this.http.get(`${this.url}`,{
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.user.token})
        }).pipe(
          map(results => { 
            this.loadServ.hide();
            return results['data'];
          }));
    }else{
      //this.loadServ.hide();
      this.authService.authenticationState.next(false);
    }
  }
}
