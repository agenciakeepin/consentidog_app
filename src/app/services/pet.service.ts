import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user.service';

import { AuthenticationService } from './authentication.service';
import { Storage } from '@ionic/storage';

import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class Pet {

  private url: string = this.authService.baseurl + 'pets';
  //private url: string = 'http://192.168.2.105:8888/consentidog/consentidog_backend/api/pets';
  //url: string = 'https://qa.keepinagency.com/consentidog_backend/v0.1.1/api/users';
  
  constructor(
    private loadServ: LoaderService,
    private http: HttpClient,
    private authService: AuthenticationService,
    private storage: Storage, 
    private user: User
  ) { }

  getMyPets(): Observable<object> {
    
    this.storage.get('auth-token').then(
      res => {
        this.user.token = res;
      }
    );
    //console.log('aqui es...');
    //console.log(this.user.token);

    if (this.user.token.length>1 && this.user.idusuario){
      return this.http.get(`${this.url}/mypets/${this.user.idusuario}`,{
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.user.token})
        }).pipe(
          map(results => { 
            this.loadServ.hide();
            return results['data'];
          }));
    }else{
      this.loadServ.hide();
      this.authService.authenticationState.next(false);
    }
  }

  addpet(datospet: any): Observable<any> { //'Content-Type': 'application/json',
    //console.log(datospet);
    let insert_pet = this.http.post<any>(`${this.url}/add` , datospet,{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.user.token})
      }).pipe(map(results => {
          console.log(results);
          //this.loadServ.hide(); 
          return results['data'];
        })
    );

    return insert_pet;

  }

  editpet(idpet: any, datosmascota: any): Observable<any> { //'Content-Type': 'application/json',
    console.log('Edit Pet en pet.service.ts datosmascota:');
    console.log(datosmascota);
    let update_pet = this.http.post<any>(`${this.url}/edit/${idpet}` , datosmascota,{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.user.token})
      }).pipe(map(results => {
          console.log(results);
          //this.loadServ.hide(); 
          return results['data'];
        })
    );

    return update_pet;

  }
}
