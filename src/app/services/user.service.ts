import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

/*import { AuthenticationService } from './authentication.service';
import { Storage } from '@ionic/storage';*/

import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class User {

  // MacJav
  //url: string = 'http://192.168.2.105:8888/consentidog/consentidog_backend/api/users';
  //url: string = 'http://localhost:8888/consentidog/consentidog_backend/api/users';
  
  // QA
  //url: string = 'https://qa.keepinagency.com/consentidog_backend/v0.1.1/api/users';
  url: string = 'https://qa.keepinagency.com/consentidog_backend/v0.2.0.1/api/users';
  //url: string = 'https://consentidog.com/backend/api/users';

  //private url: string = this.authService.baseurl + 'users';
  
  
  public token: string = ''; 
  public idusuario: '';
  public name: string = '';

  constructor(
    private loadServ: LoaderService,
    /*private authService: AuthenticationService,
    private storage: Storage,*/ 
    private http: HttpClient) {
    //private http: HTTP
    // 

     }

  getToken(accountInfo: any): Observable<any> {
    //console.log(this.user.token);  
    this.loadServ.show('Verificando información...');
    let consult_serv = this.http.post(`${this.url}/token` , accountInfo,{
                                        headers: new HttpHeaders({
                                          'Accept': 'application/json',
                                          'Content-Type': 'application/json'})
                                        }).pipe(map(results => {
                                                this.loadServ.hide(); 
                                                return results['data'];
                                        })
                                      );

    consult_serv.subscribe(res => {
                                  this._loggedIn(res);
                                  },
                           err => {
                                  this.loadServ.hide();
                                  console.log(err);
                                  } 
                           );
    
    return consult_serv;
  }

  adduser(datosusu: any): Observable<any> {
    let insert_usu = this.http.post(`${this.url}/add` , datosusu,{
      headers: new HttpHeaders({
        'Accept': 'application/json',
        'Content-Type': 'application/json'})
      }).pipe(map(results => {
          console.log(results);
          //this.loadServ.hide(); 
          return results['data'];
        })
    );

    /*insert_usu.subscribe(
      res => {
        console.log(res);
      },
      err => console.log('HTTP Error', err),
      () => console.log('HTTP request completed.')
    );*/

    return insert_usu;

  }

  getusuinfo(): Observable<any> {

    if (this.idusuario){
      //this.loadServ.show('Verificando Información...');
      return this.http.get(`${this.url}/${this.idusuario}`,{
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.token})
        }).pipe(
          map(results => {
            this.loadServ.hide();
            return results['data'];
          })
      );
    }
  } 

  _loggedIn(resp) {

    this.token = resp.token;
    this.idusuario = resp.uid;
    this.name = resp.name;
  }

  _loggedOut() {
    this.token = "";
    this.idusuario = "";
    this.name = "";
  }
}
