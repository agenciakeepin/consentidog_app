import { TestBed } from '@angular/core/testing';

import { Pet } from './pet.service';

describe('Pet', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Pet = TestBed.get(Pet);
    expect(service).toBeTruthy();
  });
});
