import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from './user.service';

import { AuthenticationService } from './authentication.service';
import { Storage } from '@ionic/storage';

import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class PetService {

  //private url: string = this.authService.baseurl + 'users';
  private url: string = this.authService.baseurl + 'pets/services';

  constructor(
    private loadServ: LoaderService,
    private http: HttpClient,
    private authService: AuthenticationService,
    private storage: Storage, 
    private user: User
  ) { 
    
  } 

  /*async datoslog(){
    //const promises = [];
    this.storage.ready().then( () =>{
      this.storage.get('auth-token').then(res => {
        this.user.token = res;
        ///promises.push(res);
        console.log(this.user.token,'this.user.token en petservice.service.ts');
        this.storage.get('uid').then(res2 => {
          //console.log(this.user.idusuario,"this.user.idusuario en petservice.service.ts");
          this.user.idusuario = res2;
          //promises.push(res); 
          return  true;
        },
        error => {
          console.log(error,'error en petservice.service.ts');
          //return false;
        });
      });
    });
    //let results = await Promise.all(promises);
    //return  true;
  }*/

  getCurServices(): Observable<object> {

    if (this.user.token.length>1 && this.user.idusuario ){
        //console.log(`${this.url}/${this.user.idusuario}`);
        return this.http.get(`${this.url}/${this.user.idusuario}`,{
          headers: new HttpHeaders({
            'Accept': 'application/json',
            'Authorization': 'Bearer ' + this.user.token})
          }).pipe(
            map(results => { 
              this.loadServ.hide();
              //console.log(results['data']);
              //return results['data']['pets'];
              return results['data'];
            }));
    }else{
        //console.log(this.user.idusuario+ ' '+this.user.token,"this.user.idusuario en petservice.service.ts");
        console.log('No tiene Token...');
        this.loadServ.hide();
        this.authService.authenticationState.next(false);
    }

  }

  

  getusuinfo(): Observable<any> {

    if (this.user.idusuario){
      //this.loadServ.show('Verificando Información...');
      return this.http.get(`${this.url}/${this.user.idusuario}`,{
        headers: new HttpHeaders({
          'Accept': 'application/json',
          'Authorization': 'Bearer ' + this.user.token})
        }).pipe(
          map(results => {
            this.loadServ.hide();
            return results['data'];
          })
      );
    }
  }  
}
