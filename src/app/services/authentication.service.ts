import { Platform } from '@ionic/angular';
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { BehaviorSubject,Observable } from 'rxjs';

import { User } from '../services/user.service'

import { ToastController } from '@ionic/angular';
 
const TOKEN_KEY = 'auth-token';

 
@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  // MacJav
  //public baseurl: string = 'http://192.168.2.105:8888/consentidog/consentidog_backend/api/';
  //public baseurl: string = 'http://localhost:8888/consentidog/consentidog_backend/api/';

  // QA
  //public baseurl: string = 'https://qa.keepinagency.com/consentidog_backend/v0.1.1/api/';
  public baseurl: string = 'https://qa.keepinagency.com/consentidog_backend/v0.2.0.1/api/';
  //public baseurl: string = 'https://consentidog.com/backend/api/';

  public authenticationState = new BehaviorSubject(false);
 
  constructor(
    private storage: Storage, 
    private plt: Platform, 
    public toastController: ToastController,
    private user: User) { 
      this.plt.ready().then(() => {
        this.checkToken();
      });
  }
 
  checkToken() {
    this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.user.token = res;
        this.storage.get('uid').then(res2 => {
          if (res2) {
            this.user.idusuario = res2;
            //console.log(res,'Pasando por CheckToken');
            this.authenticationState.next(true);
          }else{
            console.log(res2,'error en CheckToken');
          }
        });
      }else{
        this.authenticationState.next(false);
      }
    })
  }
 
  login(accountInfo: any){
    //console.log(accountInfo,'accountInfo en auth.service.ts');
    this.user.getToken(accountInfo).subscribe(
      res => {
        //console.log(res,"res en auth.serv.ts");
        if (res.token) {
          return this.storage.set(TOKEN_KEY, res.token).then(() => {
            this.user.idusuario = res.uid;
            this.user.token = res.token;
            this.storage.set('uid', res.uid);
            //console.log(res.token,'login en auth.serv.ts' );
            this.authenticationState.next(true);
            //console.log(this.authenticationState.value,'login en auth.serv.ts');
          });
        }else{
          this.authenticationState.next(false);
        }
      },(err) => {
        this.presentToast(err);
      }

    );
    
  }
  
  async presentToast(mess: any) {
  
    /*
        
    //
    // Este codigo se hizo el 01/04/2020 para probar mensaje que daba en Safari por CORS
    // Se puso a enviar mensaje para poder verificar desde el celular

    async presentToast() {*/

    const claves = Object.keys(mess);//.map(key => ({type: key, value: mess[key]}));
    const valores = Object.values(mess);
    //console.log(claves);
    //console.log(valores);
    
    let msj = '';
    if (valores[1]="401"){
        msj = 'Usuario no registrado o datos inválidos.';
    }

    const toast = await this.toastController.create({
      
      header: 'Advertencia:', //+valores[1],
      message: msj+' Por favor intente nuevamente.', //+valores[6],
      duration: 4000, 
      position: 'top', 
      color: 'secondary',
    });
    toast.present();
    
  }
 
  logout() {
    //return this.storage.remove(TOKEN_KEY).then(() => {
    return this.storage.clear().then(() => {
      this.authenticationState.next(false);
      this.user._loggedOut();
    });
  }
 
  isAuthenticated() {
    return this.authenticationState.value;
  }
 
}